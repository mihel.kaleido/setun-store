<?php

Class model extends CI_Model{

	function __construct(){
		parent::__construct();

	}

  function model(){
    $this->db->select('*');
    $this ->db->from('model');
    $query=$this->db->get();
    return $query->result_array();
  }
  function get_by_id($id){
    $this->db->select('*');
    $this ->db->from('model');
  // $this->db->join('keranjang', 'model.id = keranjang.id_model');
   $this ->db->where('id', $id);

    $query=$this->db->get();
    return $query->result_array();
  }

  function get_by_keranjang($id){
    $this->db->select('*');
    $this->db->from('keranjang');
    $this->db->join('model', 'model.id = keranjang.id_model');
    $this ->db->where('id_model', $id);
    return $this->db->get();
  }
   function get_by_user(){
    $this->db->select('*');
    $this->db->from('user');
    return $this->db->get();
  }


  function insert_into_keranjang($data){
    return $this->db->insert('keranjang', $data);
  }

  function insert_into_user($data){
    return $this->db->insert('user', $data);
  }
  function delete_user($id){ 
    $this->db->where('id',$id); 
    $this->db->delete('user'); 
    return;
  }
  function delete_order($id){ 
    $this->db->where('id',$id); 
    $this->db->delete('keranjang'); 
    return;
  }
  
}

?>
