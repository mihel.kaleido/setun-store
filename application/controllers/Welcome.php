<?php

class Welcome extends CI_Controller {
 function __construct(){
     parent::__construct();
     $this->load->model('model');
 }

	public function index()
	{
		$data['products'] = $this->db->get('model');
		
		$this->load->view('home',$data);

	}
}
