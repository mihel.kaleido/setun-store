<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_pemesanan extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('model');
	}


	public function getImage($id){
		$this->model->get_by_id($id);
	}
	public function insert_order($id_ker){
		$id_model = $this->input->post('id_model');
		$warna = $this->input->post('warna');
		$jeniskain = $this->input->post('jeniskain');
		$ketebalan = $this->input->post('ketebalan');
		$ukuran = $this->input->post('ukuran');
		$ukuransendiri = $this->input->post('ukuransendiri');
		$jumlah = $this->input->post('jumlah');

		$dataInput = array(
			'id_model' => $id_model,
			'warna' => $warna,
			'jeniskain' => $jeniskain,
			'ketebalan' => $ketebalan,
			'ukuran' => $ukuran,
			'ukuransendiri' => $ukuransendiri,
			'jumlah' => $jumlah,
		);
		$this->model->insert_into_keranjang($dataInput);
        $data['product'] = $this->model->get_by_keranjang($id_ker);
        $this->load->view('keranjang',$data);

	}
	function delete_order($id){ 
		$this->load->model('model'); 
		$this->model->delete_order($id); 
		 redirect('keranjang'); 
	}
	 
}