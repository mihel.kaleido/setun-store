<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_routing extends CI_Controller {

    function __construct(){
     parent::__construct();
     $this->load->model('model');
 }



 public function produk()
 {
   $this->load->view('produk');
} 
public function v_admin_klien_order(){
 $data['user'] = $this->db->get('user');
 $data['products'] = $this->db->get('keranjang');
$this->load->view('v_admin_klien_order',$data);
}
public function login(){
	$this->load->view('login');
}

public function check_profile($id){
$data['product'] = $this->model->get_by_id_user($id);
$this->load->view('check_profile');
}
public function kustom($id){
 $data['product'] = $this->model->get_by_id($id);
 $this->load->view('kustom',$data);
}
public function login_admin(){
    $this->load->view('login_admin');
}
public function v_admin(){
$data['user'] = $this->db->get('user');
$this->load->view('v_admin',$data);
}
public function pembayaran(){
$data['user'] = $this->model->get_by_user();
$data['product'] = $this->db->get('keranjang');
$this->load->view('pembayaran',$data);
}
public function konfirmasi(){
$data['user'] = $this->db->get('user');
$data['products'] = $this->db->get('keranjang');
$this->load->view('konfirmasi',$data);
}

public function klien_profile(){
$data['user'] = $this->db->get('user');
$this->load->view('klien_profile',$data);
}
public function klien_pesanan(){
$data['products'] = $this->db->get('keranjang');
$this->load->view('klien_pesanan',$data);
}
public function klien_kelolainformasi(){
    $this->load->view('klien_kelolainformasi');
    }
}
// public function do_upload()
// {
//     $config['upload_path']          = './uploads/';
//     $config['allowed_types']        = 'gif|jpg|png|jpeg';
//     $config['max_size']             = 100;
//     $config['max_width']            = 1024;
//     $config['max_height']           = 768;

//     $this->load->library('upload', $config);

//     if ( ! $this->upload->do_upload('userfile'))
//     {
//         $error = array('error' => $this->upload->display_errors());

//         $this->load->view('upload_form', $error);
//     }
//     else
//     {
//         $data = array('upload_data' => $this->upload->data());

//         $this->load->view('upload_success', $data);
//     }
// }
// }