<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_user extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('model');
	}

	public function insert_user($id){
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$alamat = $this->input->post('alamat');
		$telepon = $this->input->post('telepon');
		$lingkarbahu = $this->input->post('lingkarbahu');
		$lingkardada = $this->input->post('lingkardada');
		$lingkarleher = $this->input->post('lingkarleher');
		$lingkarketiak = $this->input->post('lingkarketiak');
		$lingkarperut = $this->input->post('lingkarperut');
		$lingkarpinggul = $this->input->post('lingkarpinggul');

		$dataInput = array(
			'nama' => $nama,
			'email' => $email,
			'password' => $password,
			'alamat' => $alamat,
			'telepon' => $telepon,
			'lingkarbahu' => $lingkarbahu,
			'lingkardada' => $lingkardada,
			'lingkarleher' => $lingkarleher,
			'lingkarketiak' => $lingkarketiak,
			'lingkarperut' => $lingkarperut,
			'lingkarpinggul' => $lingkarpinggul,
		);

		$this->model->insert_into_user($dataInput);
		
           redirect('');
	}


	public function getuser($id){
		$this->model->get_by_id_user($id);
	}
		function delete_user($id){ 
		$this->load->model('model'); 
		$this->model->delete_user($id); 
		redirect('c_routing/v_admin'); 
	}
	function edit_user($id){
	$where = array('id' => $id);
	$data['user'] = $this->model->edit_user($where,'user')->result();
	$this->load->view('',$data);
}
}