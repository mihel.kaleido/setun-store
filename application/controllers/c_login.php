<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class c_login extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('m_login');
    }
    public function index(){
        $session = $this->session->userdata('login'); 
        if($session != 'login'){
            $this->load->view('v_admin');
        }else{
            redirect('v_admin');
        }
    }
    // login admin
    public function administrator() {
        $username = $this->security->xss_clean($this->input->post("username"));
        $password = $this->security->xss_clean($this->input->post("password"));
        $cek = $this->m_login->cek_admin($username,$password);
        if(count($cek) == 1){
            $this->session->set_userdata(array(
                'login'         => "login",
                'username'      => $cek[0]['username'],
                'password'      => $cek[0]['password'],
                'id'      => $cek[0]['id'],
                
            ));
            redirect('c_routing/v_admin');
        }else{
            echo "<script>alert('Username atau password salah !');
            document.location='login_admin'</script>";
        }
    }
    public function logout_admin(){
        $this->session->sess_destroy();
        redirect('c_routing/login_admin');
    }
    public function login_admin(){
        $this->load->view("login_admin");
    }
    // login user
    public function user() {
        $email = $this->security->xss_clean($this->input->post("email"));
        $password = $this->security->xss_clean($this->input->post("password"));
        $cek = $this->m_login->cek_user($email,$password);
        if(count($cek) == 1){
            $this->session->set_userdata("logged_in",array(
                'login'=> "login",
                'id'=> $cek[0]['id'],
                'nama'=> $cek[0]['nama'],
                'email'=> $cek[0]['email'],
                'password'=> $cek[0]['password'],
                'alamat'=> $cek[0]['alamat'],
                'telepon'=> $cek[0]['telepon'],
                'lingkarbahu'=> $cek[0]['lingkarbahu'],
                'lingkardada'=> $cek[0]['lingkardada'],
                'lingkarleher'=> $cek[0]['lingkarleher'],
                'lingkarketiak'=> $cek[0]['lingkarketiak'],
                'lingkarperut'=> $cek[0]['lingkarperut'],
                'lingkarpinggul'=> $cek[0]['lingkarpinggul']
            ));
            $session_data = $this->session->userdata("logged_in");
            $data['username'] = $session_data['nama'];
            $data['email'] = $session_data['email'];
            $data['alamat'] = $session_data['alamat'];
            $data['telepon'] = $session_data['telepon'];
            $data['lingkarbahu'] = $session_data['lingkarbahu'];
            $data['lingkardada'] = $session_data['lingkardada'];
            $data['lingkarleher'] = $session_data['lingkarleher'];
            $data['lingkarketiak'] = $session_data['lingkarketiak'];
            $data['lingkarperut'] = $session_data['lingkarperut'];
            $data['lingkarpinggul'] = $session_data['lingkarpinggul'];
	        $data['products'] = $this->db->get('model');
           $this->load->view('klien_profile',$data);
        }else{
            echo "<script>alert('Username atau password salah !');
            document.location='login_user'</script>";
        }
    }

    public function logout_user(){
        $this->session->sess_destroy();
        redirect('Welcome/index');
    }
    public function login_user(){
        redirect('Welcome/index');
    }
}