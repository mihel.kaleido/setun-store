<!DOCTYPE html>
<html>
<head>
	<title>Taylor</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/semantic/semantic.css' ?> ">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/glidejs/dist/css/glide.core.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/glidejs/dist/css/glide.theme.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/custom-style.css' ?>">
</head>
<body style="background-color:#fff">

	<!-- navbar -->
	<div class="ui inverted segment">
		<div class="ui attached stackable inverted secondary pointing large menu">
			<div class="ui container">
				<div class="header item">
					Setun Taylor
				</div>
				<a href="<?php echo base_url(); ?>Welcome/index" class="item">
					<i class="home icon"></i> Home
				</a>
				<a href="#" class="active item">
					<i class="shopping bag layout icon"></i> Product
				</a>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
				<div class="ui login button" style=" color:#fff; background-color:transparent; margin-top: 5px;">
					<a href="<?php echo base_url(); ?>c_login/logout_user">Logout</a>
				</div>
			</div>
		</div>
	</div>
	
	<!-- end navbar -->
	<!-- end navbar -->

	<!-- content -->
	<br>
	<div class="ui grid">
		<div class="three wide column right floated">
			<div class="ui secondary vertical pointing menu">
				<a href="<?php echo base_url(); ?>c_login/klien_profile" class=" item">
					Profile
				</a>
				<a href="<?php echo base_url(); ?>c_routing/klien_pesanan" class=" active item">
					Pesanan
				</a>
				
			</div>
		</div>

		<div class="eleven wide column left floated">
			<h4>Panel Pesanan</h4>

			<div class="ui row border-top-custom"></div>

			<table class="ui single line table">
				<thead>
					<tr>
						<th>Nama Barang</th>
						<th>Deskripsi</th>
						<th>Kuantitas</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<h4 class="ui image header">
								<!-- <img src="<?php echo base_url(); ?>assets/img/bajuhitam1.jpg" class="ui massive rounded image">&emsp; -->
								<div class="content">
									<!-- Kaos -->
								</div>
							</h4>
						</td>
						
						<td>
							<?php foreach ($products->result() as $row) { ?>
							Warna : <?php echo $row->warna?><br>
							Kain : <?php echo $row->jeniskain?> <br>
							Ukuran : <?php echo $row->ukuransendiri?>
							
						</td>
						
						<td><?php echo $row->jumlah?></td>
						<?php  } ?>
						<td><i class=" icon green"></i> </td>
						
					</tbody>
				</table>

			</div>
		</div>
		<!-- end content -->
		<br>
		<br>
		<br>
		<br>
		<!-- footer -->
		<div class="ui equal width center aligned grid border-top-custom" style="margin-top: 40px;">
			<div class="row">
				<div class="column" style="text-align: left; padding: 30px 60px;">
					<h4>Setun taylor</h4>
					<p style="font-size: 11px;">Dengan keunggulan yang kami tawarkan, kami harapkan semoga anda bisa menikmati kebebasan disaat berbelanja baju/kemeja/jacket dengan ukuran yang ditentukan sendiri dan juga bahan yang kami sudah sediakan sesuai model pakaian. Kenikmatan kami adalah kepuasan anda disaat berbelanja di toko Setun Taylor.</p>
				</div>
				<div class="column" style="text-align: left; padding: 30px 60px;">
					<h4>Penjahit & Bahan</h4>
					<p style="font-size: 11px;">Penjahit yang handal dalam setiap model dan sudah terpercaya, selain itu juga bahan yang kami tawarkan dalah bahan dengan kualitas No. 1 pada setiap model.</p>
				</div>
				<div class="column" style="text-align: left; padding: 30px 60px;">
					<h4>Hubungi Kami</h4>
					<p style="font-size: 11px;">
						<i class="whatsapp icon"></i>Phone/whatsapp : +6281222333444 <br>
						<i class="marker icon"></i>Alamat : Gegerkalong Girang. 193 , Bandung
					</p>
					<button class="mini ui circular facebook icon button">
						<i class="facebook icon"></i>
					</button>
					<button class="mini ui circular twitter icon button">
						<i class="twitter icon"></i>
					</button>
					<button class="mini ui circular youtube plus icon button">
						<i class="youtube plus icon"></i>
					</button>
					<button class="mini ui circular instagram plus icon button">
						<i class="instagram plus icon"></i>
					</button>
				</div>
			</div>
			<div class="black row">
				<div class="column">
					<p style="font-size: 11px;">Setun Taylor Created By Ivan Ali Budiman <i class="copyright icon"></i>2017</p>
				</div>
			</div>
		</div>
		<!-- end footer -->

		<!-- js -->
	<<script src="<?php echo base_url().'assets/js/jquery.js' ?>"></script>
	<script src="<?php echo base_url().'assets/js/jquery-1.9.1.js' ?>"></script>
	<script src="<?php echo base_url().'assets/vendor/semantic/semantic.js' ?>"></script>
	<script src="<?php echo base_url().'assets/vendor/glidejs/dist/glide.js' ?>"></script>
	<script src="<?php echo base_url().'assets/js/semantic-custom.js' ?>"></script>

	</body>
	</html>