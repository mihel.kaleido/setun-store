<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>taylor</title>


	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/semantic/semantic.css' ?> ">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/glidejs/dist/css/glide.core.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/glidejs/dist/css/glide.theme.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/custom-style.css' ?>">
</head>
</head>
<body style="background-color:#fff">

	<!-- navbar -->
	<div class="ui inverted segment">
		<div class="ui attached stackable inverted secondary pointing large menu">
			<div class="ui container">
				<div class="header item">
					Setun Taylor
				</div>
				
				<a href="<?php echo base_url('Welcome/index'); ?>" class="item">
					<i class="home icon"></i> Home
				</a>
				<a href="#" class="active item">
					<i class="shopping bag layout icon"></i> Product
				</a>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;


				<?php
					if(isset($username)){
						echo "<a href='c_routing/klien_profile'><div class='ui  button' style=' color:#fff; background-color:transparent; height20px;'>
					<i class='user icon'>Hi, $username</i>
						</div></a>";
						echo "<div class='ui  button' style=' color:#fff; background-color:transparent; margin-top: 5px;'>
						Logout
						</div>";
					}

					else{
						echo "<div class='ui login button' style=' color:#fff; background-color:transparent; margin-top: 5px;'>
					<i class='user icon'>Login/Register</i>
						</div>";
					}

				?>
				</a>
				<a class=" right item">
					
				</a>
			</div>
		</div>
	</div>
	<!-- Modal Login -->
	<div class="ui basic fullscreen  login modal">
		<i style="color:#fff;" class="close icon"></i>
		<div class="ui grid equal width center aligned container">
			<div class=" seven wide column ">
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<h2 style="color:#fff;">Login</h2>
				<form class="ui form"  method="post" action="<?php echo base_url("c_login/user"); ?>">
					<div class="field">
						<input type="email" name="email" placeholder="email" required>
					</div>
					<div class="field">
						<input type="password" name="password" placeholder="password" required>
					</div>
					<button class="positive ui button" type="submit" style="width:100%;height:50px;font-size:20px;">Login</button>
				</form>
				<br>
				<br>
				
			</div>
			<div class="one wide column">
				<hr style="height:250px;width:0.1px" color="grey"/>
				<h5 style="color:#fff;">Atau</h5>
				<hr style="height:250px;width:0.1px" color="grey"/>
			</div>
			<div class="column">	
				<form class="ui form" method="post" action="<?php echo base_url("c_user/insert_user"); ?>">
					<div class="field"><br>
						<center><h2 style="color:#fff;">Daftar Sekarang</h2></center>
						<br>  
						
						<input class="users icon" type="text" name="nama" placeholder="nama" required>
						
					</div>
					<div class="field">
						
						<input type="email" name="email" placeholder="Email" required>
						
					</div>
					<div class="field">
						
						<input type="password" name="password" placeholder="password" min="6" required>
						
					</div>
					<div class="field">
						
						<textarea rows="3" type="text" name="alamat" placeholder="Masukan alamat lengkap" required></textarea>
						
					</div>
					<div class="field">
						
						<input type="number" min="0" name="telepon" placeholder="Telepon" required>
						
					</div>

					<hr size="" color="grey">
					<label><h3 style="color:#fff;">Masukan Ukuran</h3></label>

					<div class="fields">
						<div class="four wide field">
							<label style="color:#fff;" >Lingkar Bahu</label>
							<input placeholder="cm" type="number" min="0" name="lingkarbahu" required>
						</div>
						<div class="four wide field">
							<label style="color:#fff;" >Lingkar Dada</label>
							<input placeholder="cm" type="number" min="0" name="lingkardada" required>
						</div>
						<div class="four wide field">
							<label style="color:#fff;">Lingkar Leher</label>
							<input placeholder="cm" type="number" min="0" name="lingkarleher" required>
						</div>
					</div>
					
					<div class="fields">
						<div class="four wide field">
							<label style="color:#fff;">Lingkar Ketiak</label>
							<input placeholder="cm" type="number" min="0" name="lingkarketiak" required>
						</div>
						<div class="four wide field">
							<label style="color:#fff;">Lingkar Perut</label>
							<input placeholder="cm" type="number" min="0" name="lingkarperut" required>
						</div>
						<div class="four wide field">
							<label style="color:#fff;">Lingkar Pinggul</label>
							<input placeholder="cm" type="number" min="0" name="lingkarpinggul" required>
						</div>
						<div class="four wide field">
							<br>
						<div class="tiny ui inverted basic green button view-ukur button" style="float:right;">Lihat cara mengukur disini</div>
					</div>
					</div>
					<button class="positive ui button" type="submit" style="width:100%;height:50px;font-size:20px;">Daftar</button>
				</form>
			</div>

		</div>



	</div>
	<!-- end modal login-->
<!-- Modal View Ukur -->
	<div class="ui basic fullscreen  view-ukur modal">
		<i style="color:#fff;" class="close icon"></i>
		<video width="" height="" controls>
			<source src="<?php echo base_url().'assets/video/novemberrain.mp4' ?>">
			
		</video>

	</div>
</div>
<!-- End Modal View Ukur -->

	<!-- carousel -->
	<div id="Glide" class="glide">
		<div class="glide__arrows">
			<button class="glide__arrow prev" data-glide-dir="<">prev</button>
			
			<button class="glide__arrow next" data-glide-dir=">">next</button>
		</div>
		<div class="glide__wrapper">
			<ul class="glide__track">

				<li class="glide__slide"><img src="<?php echo base_url().'assets/img/carousel/1.jpg' ?>"></li>
				<li class="glide__slide"><img src="<?php echo base_url().'assets/img/carousel/2.jpg' ?>"></li>
				<li class="glide__slide"><img src="<?php echo base_url().'assets/img/carousel/3.jpg' ?>"></li>
				<li class="glide__slide"><img src="<?php echo base_url().'assets/img/carousel/4.jpg' ?>"></li>
			</ul>
		</div>
		<div class="glide__bullets"></div>
	</div>
	<!-- end carousel -->

	<div class="ui equal width center aligned grid container">
		<div class="eight wide column">
			<br>
			<br>
			<h2>Order or custom</h2>
			<p>Dengan model yang tersedia anda bisa memilih langsung ukuran atau bisa memilih ukuran yang tersedia ataupun bisa membuat sesuai ukuran yang anda inginkan.</p>
			<h3>Best Seller</h3>
		</div>
		

		<div class="row">
			<div class="column">
				<div class="ui centered special cards">
					<!-- card -->
					<?php foreach ($products->result() as $row) { ?>
					<div class="ui card">
						<div class="blurring dimmable image">
							<div class="ui dimmer">
								<div class="content">
									<div class="center">
										<a href="<?php echo base_url("c_routing/kustom"."/".$row->id); ?>">
											<div class="positive ui button">Beli / Kustom</div>
										</a>
									</div>
								</div>
							</div>
							<img src="<?php echo base_url("assets/img/".$row->gambar ) ?>">
						</div>
						<div class="content">
							<h3><?php echo $row->jenis?></h3>
							<div class="meta">
								<span class="date"><?php echo $row->keterangan?></span>
							</div>
							<div class="meta">
								<span class="price">Rp <?php echo $row->harga?></span>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end card -->
				</div>
			</div>
		</div>
	</div>
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->

<!-- footer -->
<div class="ui equal width center aligned grid border-top-custom">
	<div class="row">
		<div class=" column" style="text-align: left; padding: 30px 60px;">
			<h4>Setun taylor</h4>
			<p style="font-size: 11px;">Dengan keunggulan yang kami tawarkan, kami harapkan semoga anda bisa menikmati kebebasan disaat berbelanja baju/kemeja/jacket dengan ukuran yang ditentukan sendiri dan juga bahan yang kami sudah sediakan sesuai model pakaian. Kenikmatan kami adalah kepuasan anda disaat berbelanja di toko Setun Taylor.</p>
		</div>
		<div class=" column" style="text-align: left; padding: 30px 60px">
			<h4>Penjahit & Bahan</h4>
			<p style="font-size: 11px;">Penjahit yang handal dalam setiap model dan sudah terpercaya, selain itu juga bahan yang kami tawarkan dalah bahan dengan kualitas No. 1 pada setiap model.</p>
		</div>
		<div class=" column" style="text-align: left; padding: 30px 60px;">
			<h4>Hubungi Kami</h4>
			<p style="font-size: 11px;">
				<i class="whatsapp icon"></i>Phone/whatsapp : +6281222333444 <br>
				<i class="marker icon"></i>Alamat : Gegerkalong Girang. 193 , Bandung
			</p>
			<button class="mini ui circular facebook icon button">
				<i class="facebook icon"></i>
			</button>
			<button class="mini ui circular twitter icon button">
				<i class="twitter icon"></i>
			</button>
			<button class="mini ui circular youtube plus icon button">
				<i class="youtube plus icon"></i>
			</button>
			<button class="mini ui circular instagram plus icon button">
				<i class="instagram plus icon"></i>
			</button>
		</div>
	</div>
	<div class="black row">
		<div class="column">
			<p style="font-size: 11px;">Setun Taylor Created By Ivan Ali Budiman <i class="copyright icon"></i>2017</p>
		</div>
	</div>
</div>
<!-- end footer -->

<!-- js -->
<script src="<?php echo base_url().'assets/js/jquery.js' ?>"></script>
<script src="<?php echo base_url().'assets/js/jquery-1.9.1.js' ?>"></script>
<script src="<?php echo base_url().'assets/vendor/semantic/semantic.js' ?>"></script>
<script src="<?php echo base_url().'assets/vendor/glidejs/dist/glide.js' ?>"></script>
<script src="<?php echo base_url().'assets/js/semantic-custom.js' ?>"></script>
</body>
</html>