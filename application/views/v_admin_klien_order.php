<!DOCTYPE html>
<html>
<head>
	<title>Setun Taylor</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/semantic/semantic.css' ?> ">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/glidejs/dist/css/glide.core.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/glidejs/dist/css/glide.theme.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/custom-style.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/custom-style-admin.css' ?>">
</head>
<body>

	<!-- sidebar -->
	<div class="ui left fixed inverted vertical menu">
		<div class="item">
			<img class="ui image" src="<?php echo base_url(); ?>assets/img/setunTaylorWhite.png">
		</div>
		<a class="item disabled"><h3 style="color: white;">Dashboard</h3></a>
		<a href="<?php echo base_url('c_routing/v_admin'); ?>" class="item"> <i class="users icon"></i>Daftar Client</a>
		<a href="<?php echo base_url('c_routing/v_admin_klien_order'); ?>" class="active item"><i class="shopping basket icon"></i>Daftar Order</a>
		
	</div>
	<!-- end sidebar -->

	<!-- body content -->
	<div class="body-dashboard-admin">
		<div class="ui grid">
			<div class=""><br>
				<h3 style="color:white;">Selamat datang ,<br>   
					<?php echo $this->session->userdata('username'); ?></h3>
					<div class="ui compact menu">
						<div class="ui simple dropdown item">
							<i class="user icon"></i>
							<i class="dropdown icon"></i>
							<div class="menu">
								<a href="<?php echo base_url(); ?>c_login/logout_admin"><div class=" right item">Logout</div></a>
							</div>
						</div>
					</div>
				</div>


				<div class="sixteen wide column">
					
					
					<table class="ui single line table" style="margin-left: 3px;">
						<thead>
							<tr>
								<th class="collapsing"></i> No</th>
								<th></i> Deskripsi Pesanan</th>
								<th></i> Klien</th>
								<th></i>Alamat pengiriman</th>
								<th></i> Status</th>
								<th class="collapsing"><i class="options grey icon"></i> Action</th>
							</tr>
						</thead>
						<tbody>
							
							<tr>
								
								<td>1</td>
								<td>
								<h4 class="ui image header">
								<img src="<?php echo base_url(); ?>assets/img/bajuhitam1.jpg" class="ui massive rounded image"></h4>
								<b>Kaos</b><br>
								<?php foreach ($products->result() as $row) { ?>Warna :<?php echo $row->warna?> <br>
								Kain : <?php echo $row->jeniskain?> <br>
								Ukuran : <?php echo $row->ukuransendiri?></td>
								<?php  } ?>
								<?php foreach ($user->result() as $row) { ?>
								<td><?php echo $row->nama?></td>
								<td><?php echo $row->alamat?></td>
								<td></td>
								<td>
									<?php  } ?>
									<div class="ui icon small buttons">
										
										<button class="ui red basic delete-user-for-admin button" data-tooltip="Delete" data-position="top right">
											<i class="trash icon"></i>
										</button>

									</div>
								</td>
							</tr>
							
						</tbody>
				

					<!-- modal hapus -->
					<div class="ui mini delete-user-for-admin modal">
						<div class="header">
							Delete data order
						</div>
						<div class="content">
							Are you sure delete this account?
						</div>
						<div class="actions">
							<button class="ui button">Cancel</button>
							<button class="ui negative button">Delete</button>
						</div>
					</div>
					<!-- end modal hapus -->
				</div>
			</div>
		</div>
		<!-- end body content -->

		<!-- js -->
	<script src="<?php echo base_url().'assets/js/jquery.js' ?>"></script>
	<script src="<?php echo base_url().'assets/js/jquery-1.9.1.js' ?>"></script>
	<script src="<?php echo base_url().'assets/vendor/semantic/semantic.js' ?>"></script>
	<script src="<?php echo base_url().'assets/vendor/glidejs/dist/glide.js' ?>"></script>
	<script src="<?php echo base_url().'assets/js/semantic-custom.js' ?>"></script>
	</body>
	</html>